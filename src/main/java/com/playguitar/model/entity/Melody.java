package com.playguitar.model.entity;

import com.playguitar.model.exceptions.MelodyLogicalException;

import java.util.Objects;

public class Melody {
    private String name;
    private float duration;

    public Melody(String name, float duration) throws MelodyLogicalException{
        if (checkName(name) && checkDuration(duration)) {
            this.name = name;
            this.duration = duration;
        } else {
            throw new MelodyLogicalException();
        }
    }

    public Melody() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws MelodyLogicalException {
        if (checkName(name)) {
            this.name = name;
        } else {
            throw new MelodyLogicalException();
        }
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) throws MelodyLogicalException {
        if (checkDuration(duration)) {
            this.duration = duration;
        } else {
            throw new MelodyLogicalException();
        }
    }

    private boolean checkName(String name) {
        int maxString = 4;
        int maxFret = 10;
        boolean isNorm = false;
        if(name.length() == 2) {
            for(int i = 0; i < maxString; i ++) {
                if (String.valueOf(name.charAt(0)).equals(String.valueOf(i))) {
                    for (int j = 0; j < maxFret; j++) {
                        if (String.valueOf(name.charAt(1)).equals(String.valueOf(j))) {
                            isNorm = true;
                        }
                    }
                }
            }
        }
        return isNorm;
    }

    private boolean checkDuration(float duration) {
        return duration > 0 && duration < 20;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Melody melody = (Melody) o;
        return duration == melody.duration &&
                Objects.equals(name, melody.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, duration);
    }

    @Override
    public String toString() {
        return "Melody{" +
                "name='" + name + '\'' +
                ", duration=" + duration +
                '}';
    }
}
