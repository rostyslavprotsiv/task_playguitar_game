package com.playguitar.view.ui;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public abstract class KeyHandler extends KeyAdapter {
    @Override
    public abstract void keyPressed(KeyEvent e);

    @Override
    public abstract void keyReleased(KeyEvent e);
}
