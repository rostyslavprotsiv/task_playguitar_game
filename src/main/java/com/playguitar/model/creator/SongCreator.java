package com.playguitar.model.creator;

import com.playguitar.model.dao.SongDAO;
import com.playguitar.model.entity.Melody;
import com.playguitar.model.entity.Song;
import com.playguitar.model.exceptions.MelodyLogicalException;
import com.playguitar.model.exceptions.SongLogicalException;

public class SongCreator {

    public Song createSong(String name){
        Song song = null;
        try {
            if(name == "Brigada") {
                song = new Song(1, new Melody[]{
                        new Melody("23", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("14", (float) 0.5),
                        new Melody("11", (float) 0.5),
                        new Melody("12", (float) 1.5),
                        new Melody("14", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("11", (float) 1.5),
                        new Melody("12", (float) 0.5),
                        new Melody("23", (float) 2.5),
                        new Melody("23", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("14", (float) 0.5),
                        new Melody("11", (float) 0.5),
                        new Melody("12", (float) 1.5),
                        new Melody("14", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("11", (float) 1.5),
                        new Melody("21", (float) 0.5),
                        new Melody("25", (float) 2.5),
                        new Melody("23", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("14", (float) 0.5),
                        new Melody("11", (float) 0.5),
                        new Melody("12", (float) 1.5),
                        new Melody("14", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("11", (float) 1.5),
                        new Melody("12", (float) 0.5),
                        new Melody("25", (float) 2.5),
                        new Melody("23", (float) 0.5),
                        new Melody("21", (float) 0.5),
                        new Melody("14", (float) 0.5),
                        new Melody("12", (float) 0.5),
                        new Melody("14", (float) 0.5),
                        new Melody("23", (float) 1.0),
                        new Melody("21", (float) 1.0),
                        new Melody("23", (float) 0.5),
                        new Melody("25", (float) 0.5),
                        new Melody("33", (float) 1.5),
                        new Melody("23", (float) 2.5)

                }, "Brigada");
            } else if (name == "Piraty Carybskogo Morya") {
                song = new Song(2, new Melody[]{
                        new Melody("28", (float)0.5),
                        new Melody("19", (float)0.5),
                        new Melody("17", (float)1.0),
                        new Melody("17", (float)0.5),
                        new Melody("00", (float)0.5),
                        new Melody("09", (float)1.0),
                        new Melody("09", (float)1.0),
                        new Melody("09", (float)0.5),
                        new Melody("07", (float)0.5),
                        new Melody("00", (float)1.0),
                        new Melody("00", (float)1.0),
                        new Melody("17", (float)0.5),
                        new Melody("19", (float)0.5),
                        new Melody("19", (float)0.5),
                        new Melody("17", (float)1.5),
                        new Melody("28", (float)0.5),
                        new Melody("19", (float)0.5),
                        new Melody("17", (float)1.0),
                        new Melody("17", (float)0.5),
                        new Melody("00", (float)0.5),
                        new Melody("09", (float)1.0),
                        new Melody("09", (float)1.0),
                        new Melody("09", (float)0.5),
                        new Melody("07", (float)0.5),
                        new Melody("00", (float)1.0),
                        new Melody("00", (float)1.0),
                        new Melody("17", (float)0.5),
                        new Melody("19", (float)0.5),
                        new Melody("17", (float)1.5),
                        new Melody("28", (float)0.5),
                        new Melody("19", (float)0.5),
                        new Melody("17", (float)1.0),
                        new Melody("17", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("07", (float)1.0),
                        new Melody("07", (float)1.0),
                        new Melody("07", (float)0.5),
                        new Melody("05", (float)0.5),
                        new Melody("04", (float)1.0),
                        new Melody("04", (float)1.0),
                        new Melody("05", (float)0.5),
                        new Melody("07", (float)0.5),
                        new Melody("05", (float)1.0),
                        new Melody("17", (float)1.5),
                        new Melody("17", (float)0.5),
                        new Melody("00", (float)0.5),
                        new Melody("09", (float)1.0),
                        new Melody("07", (float)1.0),
                        new Melody("05", (float)0.25),
                        new Melody("17", (float)1.5),
                        new Melody("17", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("00", (float)1.0),
                        new Melody("00", (float)1.0),
                        new Melody("09", (float)0.5),
                        new Melody("17", (float)0.5),
                        new Melody("00", (float)1.5),
                        new Melody("28", (float)0.25),
                        new Melody("19", (float)0.5),
                        new Melody("00", (float)1.0),
                        new Melody("17", (float)0.25),
                        new Melody("19", (float)0.5),
                        new Melody("17", (float)0.5),
                        new Melody("17", (float)0.5),
                        new Melody("00", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("07", (float)0.5),
                        new Melody("05", (float)0.5),
                        new Melody("09", (float)0.25),
                        new Melody("17", (float)0.5),
                        new Melody("28", (float)1.0),
                        new Melody("04", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("27", (float)0.5),
                        new Melody("28", (float)0.5),
                        new Melody("17", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("05", (float)0.5),
                        new Melody("05", (float)0.5),
                        new Melody("05", (float)0.5),
                        new Melody("04", (float)0.25),
                        new Melody("05", (float)1.0),
                        new Melody("07", (float)0.5),
                        new Melody("07", (float)0.5),
                        new Melody("07", (float)0.5),
                        new Melody("07", (float)0.25),
                        new Melody("05", (float)1.0),
                        new Melody("05", (float)0.5),
                        new Melody("05", (float)0.5),
                        new Melody("04", (float)0.25),
                        new Melody("05", (float)1.0),
                        new Melody("07", (float)0.5),
                        new Melody("09", (float)0.5),
                        new Melody("00", (float)0.5),
                        new Melody("17", (float)1.0) }, "Piraty Carybskogo Morya");
            }
        } catch (MelodyLogicalException e) {
            e.printStackTrace();
        } catch (SongLogicalException e) {
            e.printStackTrace();
        }
        return song;
    }

    public void addSongsToDB() {
        try(SongDAO dao = new SongDAO()) {
            dao.create(createSong("Brigada"));
            dao.create(createSong("Piraty Carybskogo Morya"));
        }
    }
}
