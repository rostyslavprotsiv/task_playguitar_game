package com.playguitar.view.ui.menu;

import com.playguitar.view.Menu;
import com.playguitar.view.ui.ConsoleHandler;
import com.playguitar.view.ui.MyConsole;

public class StartEntry extends FirstMenuEntry {
    private String options;

    public StartEntry() {
        options = "Start the free mode";
        this.setMenuEntry(options);
    }

    @Override
    public void run(MyConsole myConsole) {
        Menu fMenu = new Menu();
        ConsoleHandler menuController = new ConsoleHandler(myConsole, fMenu);
        myConsole.setKeyHandler(menuController.getKeyHandler());
    }
}

