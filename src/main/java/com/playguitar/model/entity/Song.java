package com.playguitar.model.entity;

import com.playguitar.model.exceptions.SongLogicalException;

import java.util.Arrays;
import java.util.Objects;

public class Song {
    private int id;
    private Melody[] sounds;
    private String name;

    public Song(int id, Melody[] sounds, String name) throws SongLogicalException {
        if (checkId(id)) {
            this.id = id;
            this.sounds = sounds;
            this.name = name;
        } else {
            throw new SongLogicalException();
        }
    }

    public Song(Melody[] sounds, String name) {
        this.sounds = sounds;
        this.name = name;
    }

    public Song() {
    }

    public Melody[] getSounds() {
        return sounds;
    }

    public void setSounds(Melody[] sounds) {
        this.sounds = sounds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) throws SongLogicalException {
        if (checkId(id)) {
            this.id = id;
        } else {
            throw new SongLogicalException();
        }
    }

    public float getDuration() {
        float duration = 0;
        for(Melody mel : sounds) {
            duration += mel.getDuration();
        }
        return duration;
    }

    private boolean checkId(int id) {
        return id > 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return id == song.id &&
                Arrays.equals(sounds, song.sounds) &&
                Objects.equals(name, song.name);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(id, name);
        result = 31 * result + Arrays.hashCode(sounds);
        return result;
    }

    @Override
    public String toString() {
        return "Song{" +
                "id=" + id +
                ", sounds=" + Arrays.toString(sounds) +
                ", name='" + name + '\'' +
                '}';
    }
}

