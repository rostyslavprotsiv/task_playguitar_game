package com.playguitar.view;


public class ConstantsForGuitar {
    static final String START_OF_FIRST_STRING = "      \\";
    static final String START_OF_SECOND_STRING = "       ";
    static final String START_OF_THIRD_STRING = "({O})  ";
    static final String START_OF_FOURTH_STRING = "       ";
    static final String STATIC_PART_OF_FIRST = "______./_____.______.____._____.____.__._";
    static final String STATIC_PART_OF_OTHER = "______|______|______|____|_____|____|__||";
    static final String DYNAMIC_PART_OF_FIRST = "/_____________________________";
    static final String DYNAMIC_PART_OF_SECOND = "______________________________";
    static final String DYNAMIC_PART_OF_THIRD = "______________________________";
    static final String DYNAMIC_PART_OF_FOURTH = "______________________________";
    static final String CHANGED_PART_OF_FIRST = "/*****************************";
    static final String CHANGED_PART_OF_SECOND = "***********************";
    static final String CHANGED_PART_OF_THIRD = "******************************";
    static final String CHANGED_PART_OF_FOURTH = "*******************************";
}