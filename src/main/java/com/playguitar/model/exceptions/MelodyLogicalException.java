package com.playguitar.model.exceptions;

public class MelodyLogicalException extends Exception {

    public MelodyLogicalException() {
    }

    public MelodyLogicalException(String s) {
        super(s);
    }

    public MelodyLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public MelodyLogicalException(Throwable throwable) {
        super(throwable);
    }

    public MelodyLogicalException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
