package com.playguitar.view.ui.menu;

import com.playguitar.view.Menu;
import com.playguitar.view.ui.ConsoleHandler;
import com.playguitar.view.ui.MyConsole;

public class AllEntry extends FirstMenuEntry {
    private String option;

    public AllEntry() {
        option = "Play all guitar";
        this.setMenuEntry(option);
    }

    @Override
    public void run(MyConsole myConsole) {
        Menu fMenu = new Menu();
        fMenu.playAllGuitar();
        ConsoleHandler menuController = new ConsoleHandler(myConsole, fMenu);
        myConsole.setKeyHandler(menuController.getKeyHandler());
    }
}
