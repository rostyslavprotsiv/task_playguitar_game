package com.playguitar.view.ui.menu;

import com.playguitar.view.ui.MyConsole;

import java.util.ArrayList;

public class FirstMenu {
    private MyConsole myConsole;
    private ArrayList<FirstMenuEntry> entryArrayList;

    public FirstMenu(MyConsole console) {
        entryArrayList = new ArrayList<>();
        myConsole = console;
    }

    public final void initializeEntries() {
        entryArrayList.add(new StartEntry());
        entryArrayList.add(new RepeatEntry());
        entryArrayList.add(new AllEntry());
        entryArrayList.add(new ExitEntry());
    }

    public final void printMenu() {
        StringBuilder menuEntry = new StringBuilder();
        menuEntry.append("Welcome to Guitar Hero!\n"
                + "Your options:\n");
        for (int i = 0; i < entryArrayList.size(); i++) {
            menuEntry.append((i + 1 + ") " + entryArrayList.get(i).getMenuEntry() + "\n"));
        }
        myConsole.setText(menuEntry.toString());
    }
}
